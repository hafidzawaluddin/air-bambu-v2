<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Start Meta -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Air Bambu Habiburrahman" />
    <meta name="keywords" content="Creative, Digital, multipage, landing, freelancer template" />
    <meta name="author" content="ThemeOri">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title of Site -->
    <title>Air Bambu Habiburrahman</title>
    <!-- Favicons -->
    <link rel="icon" type="image/png" href="{{ secure_asset('assets/img/favicon.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ secure_asset('assets/css/bootstrap.min.css') }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ secure_asset('assets/css/animate.css') }}">
    <!-- Swiper -->
    <link rel="stylesheet" href="{{ secure_asset('assets/css/swiper-bundle.min.css') }}">
    <!-- Magnific -->
    <link rel="stylesheet" href="{{ secure_asset('assets/css/magnific-popup.css') }}">
    <!-- Mean menu -->
    <link rel="stylesheet" href="{{ secure_asset('assets/css/meanmenu.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ secure_asset('assets/sass/style.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

</head>

<body>
    <!-- Preloader start -->
    <div class="theme-loader">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!-- Preloader end -->
    <!-- Header Area Start -->
    <div class="header__sticky one">
        <div class="header__area">
            <div class="container custom__container">
                <div class="header__area-menubar">
                    <div class="header__area-menubar-left">
                        <div class="header__area-menubar-left-logo">
                            <a href="index.html"><img class="lazyload" data-src="{{ secure_asset('assets/img/logo.png') }}"
                                    alt=""></a>
                            <div class="responsive-menu"></div>
                        </div>
                        <div class="header__area-menubar-left-contact">
                            <div class="header__area-menubar-left-contact-icon">
                                <i class="fa-brands fa-whatsapp text-dark"></i>
                            </div>
                            <div class="header__area-menubar-left-contact-info">
                                <h6><a href="https://wa.me/628170222233" target="_blank" class="text-dark">Kontak
                                        0817-0222-233</a></h6>
                            </div>
                        </div>
                    </div>
                    <div class="header__area-menubar-right">
                        <div class="header__area-menubar-right-menu menu-responsive two">
                            <ul id="mobilemenu">
                                <li class="active"><a class="text-dark" href="#">Beranda</a>
                                </li>
                                <li class="menu-item-has-children"><a class="text-dark" href="#">Tentang Kami</a>
                                    <ul class="sub-menu">
                                        <li><a class="text-dark" href="{{ route('sejarah') }}">Sejarah Air Bambu</a>
                                        </li>
                                        <li><a class="text-dark" href="{{ route('sertifikasi') }}">Sertifikasi</a></li>
                                    </ul>
                                </li>
                                <li><a class="text-dark" href="{{ route('produk') }}">Produk</a>
                                </li>
                                <li><a class="text-dark" href="{{ route('berita') }}">Berita</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Area End -->
    <!-- Banner Area Start -->
    <div class="home__banner">
        <div class="banner__slide ">
            <div class="swiper-wrapper">
                <div class="banner__slide-area swiper-slide">
                    <div class="banner__slide-area-image"
                        data-background="{{ secure_asset('assets/img/bg/Website-Design.jpg') }}"></div>
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-last order-lg-first">
                                <div class="banner__slide-content">
                                    <span class="subtitle__one text-dark">Sehat Alami bersama</span>
                                    <h1 data-delay=".6s" class="text-green">Air Bambu Habiburrahman</h1>
                                    <div class="banner__slide-content-button" data-delay=".9s">
                                        <a href="{{ route('produk') }}" class="theme-banner-btn">Tampilkan Semua Produk<i
                                                class="fa-solid fa-chevrons-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Area End -->
    <!-- About Area Start -->
    <div class="about__area section-padding">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6 lg-mb-30">
                    <div class="about__area-left">
                        <div class="about__area-left-image">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/about/about-1.png') }}" alt="">
                            <div class="about__area-left-image-two">
                                <img class="img__full" src="{{ secure_asset('assets/img/about/about-2.png') }}"
                                    alt="">
                            </div>
                            <div class="about__area-left-image-three">
                                <img class="img__full" src="{{ secure_asset('assets/img/about/about-3.png') }}"
                                    alt="">
                            </div>
                            <div class="about__area-left-image-shape">
                                <img class="img__full" src="{{ secure_asset('assets/img/shape/about.png') }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="about__area-right">
                        <div class="about__area-right-title">
                            <span class="subtitle__one">Tentang Kami</span>
                            <h2>Air Bambu Habiburrahman</h2>
                            <p>Khasiat air bambu yang sudah dipercaya sejak 2500 tahun lalu. Warisan alam yang kaya akan
                                manfaat kini disajikan dalam satu botol Air Bambu Habiburrahman</p>
                        </div>
                        <div class="about__area-right-bottom mt-35">
                            <h5>Air Bambu Habiburrahman untuk hidup sehat alami anda</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Area End -->
    <!-- Services Area Start -->
    <!-- <div class="services__area section-padding">
  <div class="container">
   <div class="row mb-50">
    <div class="col-xl-12">
     <div class="services__area-title">
      <span class="subtitle__two">Services</span>
      <span class="subtitle__one">Our Services</span>
      <h2>Popular Hair Cutting And salon</h2>
     </div>
    </div>
   </div>
   <div class="row">
    <div class="col-xl-12 mb-35">
     <div class="services__area-button">
      <ul class="nav nav-pills">
       <li class="nav-item">
        <button class="nav-item-button" data-bs-toggle="pill" data-bs-target="#haircut">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-1.png') }}" alt="">
          <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-7.png') }}" alt="">
         Trend Haircut
        </button>
       </li>
       <li class="nav-item" role="presentation">
        <button data-bs-toggle="pill" data-bs-target="#washing">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-2.png') }}" alt="">
         <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-8.png') }}" alt="">
         Hair Washing
        </button>
       </li>
       <li class="nav-item" role="presentation">
        <button data-bs-toggle="pill" data-bs-target="#color">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-3.png') }}" alt="">
         <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-9.png') }}" alt="">
         Hair Color
        </button>
       </li>
       <li class="nav-item" role="presentation">
        <button class="active" data-bs-toggle="pill" data-bs-target="#facial">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-4.png') }}" alt="">
         <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-10.png') }}" alt="">
         Facial hair trim
        </button>
       </li>
       <li class="nav-item" role="presentation">
        <button data-bs-toggle="pill" data-bs-target="#shave">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-5.png') }}" alt="">
         <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-11.png') }}" alt="">
         Lather shave
        </button>
       </li>
       <li class="nav-item" role="presentation">
        <button data-bs-toggle="pill" data-bs-target="#massage">
         <img class="nav-item-button-icon" src="{{ secure_asset('assets/img/icon/services-6.png') }}" alt="">
         <img class="nav-item-button-icon2" src="{{ secure_asset('assets/img/icon/services-12.png') }}" alt="">
         Head Massage
        </button>
       </li>
      </ul>
     </div>
    </div>
    <div class="col-xl-12">
     <div class="tab-content">
      <div class="tab-pane fade" id="haircut">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-1.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="tab-pane fade" id="washing">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-2.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="tab-pane fade" id="color">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-3.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="tab-pane fade show active" id="facial">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-4.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="tab-pane fade" id="shave">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-5.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="tab-pane fade" id="massage">
       <div class="row align-items-center">
        <div class="col-xl-6 col-lg-6 lg-mb-30">
         <div class="services__area-image">
          <img class="img__full" src="{{ secure_asset('assets/img/features/services-6.jpg') }}" alt="">
         </div>
        </div>
        <div class="col-xl-6 col-lg-6">
         <div class="services__area-right ml-40 lg-ml-0">
          <div class="services__area-right-title">
           <h3>Best Facial Hair Trim At Home Treatment</h3>
           <p>Phasellus vitae purus ac urna consequat facilisis a vel leo. Maecenas hendrerit lacinia mollis. Fusce in leo lectus. Phasellus leo mi, pellentesque nec risus malesuada, volutpat porta ligula.</p>
           <div class="services__area-right-title-list">
                            <span><i class="far fa-check"></i>Easy to use salon special offer navigation</span>
                            <span><i class="far fa-check"></i>We care about our customers satisfaction</span>
           </div>
           <a href="contact.html" class="theme-btn mt-50">Booking Appointment<i class="fa-solid fa-chevrons-right"></i></a>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div> -->
    <!-- Services Area End -->
    <!-- Booking Area Start -->
    <div class="booking__area section-padding lazyload" data-background="{{ secure_asset('assets/img/bg/booking.jpg') }}">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-7 col-lg-8 lg-mb-30">
                    <div class="booking__area-title lg-t-center">
                        <span class="subtitle__one">Kontak Kami</span>
                        <h2>Pemesanan & <br>Konsultasi</h2>
                        <a href="https://wa.me/628170222233" target="_blank" class="theme-banner-btn mt-40">Hubungi<i
                                class="fa-solid fa-chevrons-right"></i></a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-4">
                    <div class="booking__area-right t-right lg-t-center">
                        <div class="booking__area-right-contact">
                            <div class="booking__area-right-contact-icon">
                                <i class="fa-brands fa-whatsapp"></i>
                            </div>
                            <div class="booking__area-right-contact-content">
                                <span>WhatsApp</span>
                                <h4><a href="https://wa.me/628170222233" target="_blank">0817-0222-233</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Booking Area End -->
    <!-- Products Area Start -->
    <div class="products__area section-padding">
        <div class="container">
            <div class="row mb-65">
                <div class="col-xl-12">
                    <div class="products__area-title">
                        <h2>Produk Kami</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 md-mb-30">
                    <div class="products__area-item">
                        <div class="">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-1.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 md-mb-30">
                    <div class="products__area-item">
                        <div class="">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-4.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="products__area-item">
                        <div class="">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-3.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Products Area End -->
    <!-- Working Area Start -->
    <!-- Working Area End -->
    <!-- Testimonial Area Start -->
    <div class="testimonial__area section-padding">
        <div class="container">
            <div class="row mb-65">
                <div class="col-xl-12">
                    <div class="products__area-title">
                        <h2>Testimoni</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6 md-mb-30">
                    <div class="testimonial__area-item">
                        <div class="testimonial__area-item-content">
                            <p class="fst-italic">"Alhamdulillah setelah pemakaian rutin seminggu dan cek ke dokter gula saya turun.
                                Terimakasih kak"</p>
                            <h5>Sakit Gula Darah</h5>
                        </div>
                        <div class="testimonial__area-item-icon">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/icon/quote.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 md-mb-30">
                    <div class="testimonial__area-item">
                        <div class="testimonial__area-item-content">
                            <p class="fst-italic">"Pemakaian 4 hari tubuh saya segar pak, adik saya yang kecil juga aman dan bisa ikut
                                minum"</p>
                            <h5>Kebugaran Tubuh</h5>
                        </div>
                        <div class="testimonial__area-item-icon">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/icon/quote.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="testimonial__area-item">
                        <div class="testimonial__area-item-content">
                            <p class="fst-italic">"Benjolan di tubuh anak saya sudah mereda karena rutin minum Air Bambu Habiburrahman,
                                alhamdulillah"</p>
                            <h5>Kelenjar Getah Bening</h5>
                        </div>
                        <div class="testimonial__area-item-icon">
                            <img class="lazyload" data-src="{{ secure_asset('assets/img/icon/quote.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial Area End -->
    <!-- Blog Area Start -->
    <!-- Blog Area End -->
    <!-- Instagram Area Start -->
    <div class="instagram__area one pt-5 pb-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5 lg-mb-10">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-1.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5 sm-mb-10">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-2.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5 sm-mb-10">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-3.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5 sm-mb-10">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-4.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5 sm-mb-10">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-5.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-4 pl-5 pr-5">
                    <div class="instagram__area-item">
                        <img class="lazyload" data-src="{{ secure_asset('assets/img/features/instagram-6.jpg') }}" alt="">
                        <div class="instagram__area-item-icon">
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Instagram Area End -->
    <!-- Footer Area Start -->
    @include('components.footer')
