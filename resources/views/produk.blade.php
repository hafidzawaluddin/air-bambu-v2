@extends('components.empty')

@section('content')
    <div class="page__banner" data-background="{{ secure_asset('assets/img/bg/produk-banner.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__banner-title">
                        <h1 class="text-green">Produk Kami</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product__details section-padding pb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="product__details-title xl-ml-0 mb-4">
                        <h3 class="text-center">Air Bambu Habiburrahman</h3>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-6 order-first order-sm-1">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-1.jpg') }}" alt="">
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-6 order-first order-sm-1">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-2.jpg') }}" alt="">
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-6 order-first order-sm-1">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/products/product-3.jpg') }}" alt="">
                </div>
            </div>
            <div class="row mt-65">
                <div class="col-xl-12">
                    <div class="product__details-description">
                        <div class="product__details-information nav">
                            <h6 class="nav-button text-dark active" data-bs-toggle="tab" data-bs-target="#description">
                                Deskripsi</h6>
                            <h6 class="nav-button text-dark" data-bs-toggle="tab" data-bs-target="#information">Informasi
                                Produk
                            </h6>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="description">
                                <p class="mb-20">
                                    Air Bambu Habiburrahman adalah minuman air bambu asli dengan kandungan 100% air bambu
                                    murni tanpa pemanis dan pengawet tambahan. Mengandung mineral dan oksigen serta
                                    antioksidan yang baik untuk tubuh dan mampu menyembuhkan berbagai penyakit, mempercepat
                                    pemulihan serta baik untuk menjaga kekebalan imun tubuh.
                                    Kandungan baik dan alami dari air bambu dalam Air Bambu Habiburrahman sudah dipercaya
                                    untuk menyembuhkan darah tinggi, vertigo, diabetes, amandel, kesulitan BAB, maag,
                                    insomnia dan barbagai penyakit lainnya.
                                </p>
                                <p>Aturan Pemakaian</p>
                                <ol>
                                    <li><span class="text-dark">Produk yang telah dibuka dapat bertahan hingga 2 hari
                                            dalam suhu ruangan</span></li>
                                    <li><span class="text-dark">Dapat diminum minimal 3x sehari dalam keadaan perut kosong
                                            atau beberapa jam sebelum makan
                                        </span></li>
                                </ol>
                            </div>
                            <div class="tab-pane fade" id="information">
                                <div class="product__details-product-information">
                                    <ul>
                                        <li><span class="text-dark">Harga :</span>Rp 25.000 / botol</li>
                                        <li><span class="text-dark">Ukuran :</span>120mL / botol</li>
                                        <li><span class="text-dark">DINKES. NO. PIRT :</span>2133204011444</li>
                                        <li><span class="text-dark">Halal MUI JB NO :</span>01121208560178</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
