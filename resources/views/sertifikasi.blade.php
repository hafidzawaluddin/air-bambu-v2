@extends('components.empty')

@section('content')
    <div class="page__banner" data-background="{{ asset('assets/img/bg/sertifikasi.png') }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__banner-title">
                        <h1 class="text-green">Sertifikasi</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product__page section-padding">
      <div class="container">
        <div class="row py-4">
          <div class="col">
            <img class="lazyload" data-src="{{ secure_asset('assets/img/sertifikasi/sertifikat-1.png') }}" alt="logo-image">
          </div>
          <div class="col">
            <img class="lazyload" data-src="{{ secure_asset('assets/img/sertifikasi/sertifikat-2.png') }}" alt="logo-image">
          </div>
        </div>
      </div>
    </div>
@endsection
