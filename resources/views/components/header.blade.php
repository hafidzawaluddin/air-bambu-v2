<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Start Meta -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="Air Bambu Habiburrahman"/>
	<meta name="keywords" content="air bambu, minuman herbal, air bambu habiburrahman, air bambu habiburrahman asli, sehat, alami, minumam yang menyembuhkan penyakit, testimoni air bambu habiburrahman, sejarah air bambu habiburrahman, produk air bambu, visi misi air bambu habiburrahman, agen air bambu habiburrahman, minumam herbal kota bandung"/>
	<meta name="author" content="Air Bambu Habiburrahman">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Title of Site -->
	<title>Air Bambu Habiburrahman</title>
	<!-- Favicons -->
	<link rel="icon" type="image/png" href="{{ secure_asset('assets/img/favicon.png') }}">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/bootstrap.min.css') }}">
	<!-- font awesome -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/all.css') }}">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/animate.css') }}">
	<!-- Swiper -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/swiper-bundle.min.css') }}">
	<!-- Magnific -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/magnific-popup.css') }}">
	<!-- Mean menu -->
	<link rel="stylesheet" href="{{ secure_asset('assets/css/meanmenu.min.css') }}">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="{{ secure_asset('assets/sass/style.css') }}"> 
	<link rel="stylesheet" href="{{ secure_asset('assets/css/custom.css') }}"> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/brands.min.css">
</head>