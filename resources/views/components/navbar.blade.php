<div class="header__sticky two">
    <div class="header__area two">
        <div class="container custom__container">
            <div class="header__area-menubar">
                <div class="header__area-menubar-left">
                    <div class="header__area-menubar-left-logo">
                        <a href="{{ route('index') }}"><img src="{{ secure_asset('assets/img/logo.png') }}"
                                alt=""></a>
                        <div class="responsive-menu"></div>
                    </div>
                    <div class="header__area-menubar-left-contact">
                        <div class="header__area-menubar-left-contact-icon">
                            <i class="fal fa-phone-alt text-dark"></i>
                        </div>
                        <div class="header__area-menubar-left-contact-info">
                            <h6><a href="https://wa.me/628170222233" target="_blank" class="text-dark">Kontak
                                    0817-0222-233</a></h6>
                        </div>
                    </div>
                </div>
                <div class="header__area-menubar-right">
                    <div class="header__area-menubar-right-menu menu-responsive two">
                        <ul id="mobilemenu">
                            <li><a class="text-dark" href="{{ route('index') }}">Beranda</a>
                            </li>
                            <li class="menu-item-has-children"><a class="text-dark" href="#">Tentang Kami</a>
                                <ul class="sub-menu">
                                    <li><a class="text-dark" href="{{ route('sejarah') }}">Sejarah Air Bambu</a></li>
                                    <li><a class="text-dark" href="{{ route('sertifikasi') }}">Sertifikasi</a></li>
                                </ul>
                            </li>
                            <li><a class="text-dark" href="{{ route('produk') }}">Produk</a>
                            </li>
                            <li><a class="text-dark" href="{{ route('berita') }}">Berita</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
