<div class="footer__area pt-0">
    <div class="footer__area-shape">
        <img dat-src="{{ secure_asset('assets/img/shape/foorer.png') }}" alt="">
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-7 col-lg-7 lg-mb-30">
                <div class="footer__area-left">
                    <div class="footer__area-left-logo">
                        <a href="index.html"><img class="lazyload" data-src="{{ secure_asset('assets/img/logo.png') }}" alt=""></a>
                    </div>
                    <div class="footer__area-left-menu">
                        Perum Blok H-130 Desa Cinunuk, Kecamatan Cileunyi Cinunuk Cileunyi Bandung Jawa Barat 40393 ID,
                        Jl. Permata Biru, Cinunuk, Kec. Cileunyi, Kabupaten Bandung, Jawa Barat 40624
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright__area mt-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6 col-md-7 md-mb-10">
                    <div class="copyright__area-left md-t-center">
                        <p class="text-dark">Copyright © 2023<a href="index.html"> Air Bambu Habiburrahman</a></p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-5">
                    <div class="copyright__area-right t-right md-t-center">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-snapchat"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Area End -->
<!-- Scroll Btn End -->
<!-- Main JS -->
<script src="{{ secure_asset('assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ secure_asset('assets/js/bootstrap.min.js') }}"></script>
<!-- Counter up -->
<script src="{{ secure_asset('assets/js/jquery.counterup.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ secure_asset('assets/js/popper.min.js') }}"></script>
<!-- Magnific JS -->
<script src="{{ secure_asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<!-- Swiper JS -->
<script src="{{ secure_asset('assets/js/swiper-bundle.min.js') }}"></script>
<!-- Waypoints JS -->
<script src="{{ secure_asset('assets/js/jquery.waypoints.min.js') }}"></script>
<!-- Mean menu -->
<script src="{{ secure_asset('assets/js/jquery.meanmenu.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ secure_asset('assets/js/custom.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js">
</script>
<script>
    $(function() {
        $('.lazyload').Lazy({
            scrollDirection: 'vertical',
            effect: 'fadeIn',
            visibleOnly: true,
            onError: function(element) {
                console.log('error loading ' + element.data('src'));
            },
						afterLoad: function(element) {
							console.log('loaded');
		        },
        });

    });
</script>
</body>
