@include('components.header')


<body>
	<!-- Preloader start -->
	<div class="theme-loader">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>

  @include('components.navbar')
  @yield('content')
  @include('components.footer')
</body>
