@extends('components.empty')

@php
    use Carbon\Carbon;
@endphp

@section('content')
    <div class="page__banner" data-background="{{ Storage::url($blog->banner_image) }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__banner-title">
                        <h1>{{ $blog->banner_title }} </h1>
                        <div class="page__banner-title-menu">
                            <ul>
                                <li><a href="{{ route('index') }}" class="text-success">Home</a></li>
                                <li class="text-success"><span> / </span> {{ $blog->title }} </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Banner End -->
    <!-- Blog Details Start -->
    <div class="blog__details section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 lg-mb-30">
                    <div class="blog__details-left">
                        <img src="{{ Storage::url($blog->header_image) }}" alt="">
                        <div class="blog__details-left-meta">
                            <ul>
                                <li><a href="#"><i class="fal fa-user"></i>By - {{ $blog->writter }} </a></li>
                                <li><a href="#"><i class="fal fa-calendar-alt"></i>{{ Carbon::parse($blog->created_at)->format('j F, Y'); }}</a></li>
                            </ul>
                        </div>
                        {!! $blog->content !!}
                        <div class="blog__details-left-related">
                            <div class="row align-items-center">
                                <div class="col-md-7 md-mb-10">
                                    <div class="blog__details-left-related-tag">
                                        <h6>Tags :</h6>
                                        <ul>
                                            @foreach (json_decode($blog->tags) as $item)
                                              <li><a href="#" class="">{{ $item }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 t-right md-t-left">
                                    <div class="blog__details-left-related-share">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fab fa-behance"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fab fa-youtube"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
