@extends('cms.base')

@section('content')
    <div class="page-header">
        <h1 class="page-title">Blog</h1>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h3 class="card-title">List Blog</h3>
                    <div class="float-right">
                        <a href="{{ route('cms.blog.create') }}" class="btn btn-primary btn-block float-end my-2"><i class="fa fa-plus-square me-2"></i>New Blogs</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table border text-nowrap text-md-nowrap mb-0">
                            <thead class="table-primary">
                                <tr>
                                    <th>Judul</th>
                                    <th>Banner</th>
                                    <th>Tags</th>
                                    <th>Penulis</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $result)
                                    <tr>
                                        <td>{{ $result->title }}</td>
                                        <td><img src="{{ Storage::url($result->banner_image)  }}" width="180px" height="90px"></td>
                                        <td>{{ $result->tags }}</td>
                                        <td>{{ $result->writter }}</td>
                                        <td>
                                            <a href="{{ route('cms.blog.edit', $result->id) }}" class="btn btn-icon btn-sm btn-warning"><span class="svg-icon svg-icon-1"><i class="bi bi-pencil-square"></i></span></a>
                                            <button type="submit" form="form-delete-2a8c17fc-4b6d-4216-84c2-eab1cfcfb118" class="btn btn-icon btn-sm btn-danger"><span class="svg-icon svg-icon-1"><i class="bi bi-x-lg"></i></span></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
