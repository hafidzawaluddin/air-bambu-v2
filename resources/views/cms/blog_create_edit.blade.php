@extends('cms.base')

@section('content')
    <div class="page-header">
        <h1 class="page-title">Blog</h1>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h3 class="card-title">Create / Edit Blog</h3>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" id="form-post" action="{{ route('cms.blog.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname">Judul</label>
                                    <input type="text" class="form-control" id="exampleInputname" name="title">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname1">Judul Banner</label>
                                    <input type="text" class="form-control" id="exampleInputname1" name="banner_title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname">Meta Keywords</label>
                                    <input type="text" class="form-control" id="exampleInputname" name="meta_keywords">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname1">Meta Kewords</label>
                                    <input type="text" class="form-control" id="exampleInputname1"
                                        name="meta_description">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label class="form-label mt-0">Banner Image</label>
                                    <input class="form-control" name="banner_image" type="file" accept="image/*">
                                    <br>
                                    <img src="{{ Storage::url($blog->banner_image)  }}" width="180px" height="90px">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname1">Header Image</label>
                                    <input class="form-control" name="header_image" type="file" accept="image/*">
                                    <br>
                                    <img src="{{ Storage::url($blog->header_image)  }}" width="180px" height="90px">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputname1">Tags</label>
                                    <select class="form-control" id="tags" multiple name="tags[]">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Content</label>
                            <textarea name="content" id="summernote"></textarea>
                        </div>
                    </form>
                </div>
                <div class="card-footer text-end">
                    <a href="javascript:void(0)" class="btn btn-danger my-1">Cancel</a>
                    <button class="btn btn-success my-1" type="submit" form="form-post">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#tags").select2({
            tags: true
        });
    </script>
@endsection