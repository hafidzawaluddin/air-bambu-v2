<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-md-12 col-sm-12 text-center">
                Copyright © <span id="year"></span> <a href="javascript:void(0)">Sash</a>. Designed with <span class="fa fa-heart text-danger"></span> by <a href="javascript:void(0)"> Spruko </a> All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- FOOTER CLOSED -->
</div>

<!-- BACK-TO-TOP -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<!-- JQUERY JS -->
<script src="{{ secure_asset('assets/cms/assets/js/jquery.min.js') }}"></script>

<!-- BOOTSTRAP JS -->
<script src="{{ secure_asset('assets/cms/assets/plugins/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ secure_asset('assets/cms/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- TypeHead js -->
<script src="{{ secure_asset('assets/cms/assets/plugins/bootstrap5-typehead/autocomplete.js') }}"></script>
<script src="{{ secure_asset('assets/cms/assets/js/typehead.js') }}"></script>

<!-- Perfect SCROLLBAR JS-->
<script src="{{ secure_asset('assets/cms/assets/plugins/p-scroll/perfect-scrollbar.js') }}"></script>
<script src="{{ secure_asset('assets/cms/assets/plugins/p-scroll/pscroll.js') }}"></script>
<script src="{{ secure_asset('assets/cms/assets/plugins/p-scroll/pscroll-1.js') }}"></script>

<!-- SIDE-MENU JS -->
<script src="{{ secure_asset('assets/cms/assets/plugins/sidemenu/sidemenu.js') }}"></script>

<!-- SIDEBAR JS -->
<script src="{{ secure_asset('assets/cms/assets/plugins/sidebar/sidebar.js') }}"></script>

<!-- Color Theme js -->
<script src="{{ secure_asset('assets/cms/assets/js/themeColors.js') }}"></script>

<!-- Sticky js -->
<script src="{{ secure_asset('assets/cms/assets/js/sticky.js') }}"></script>

<!-- CUSTOM JS -->
<script src="{{ secure_asset('assets/cms/assets/js/custom.js') }}"></script>

<!-- Custom-switcher -->
<script src="{{ secure_asset('assets/cms/assets/js/custom-swicher.js') }}"></script>

<!-- Switcher js -->
<script src="{{ secure_asset('assets/cms/assets/switcher/js/switcher.js') }}"></script>


<script src="{{ secure_asset('assets/cms/assets/plugins/summernote/summernote1.js') }}"></script>
<script src="{{ secure_asset('assets/cms/assets/js/summernote.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@yield('script')
</body>

</html>