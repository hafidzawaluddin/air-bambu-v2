<!doctype html>
<html lang="en" dir="ltr">

<head>

    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ secure_asset('assets/cms/assets/images/brand/favicon.ico') }}">

    <!-- TITLE -->
    <title>Air Bambu CMS</title>

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="{{ secure_asset('assets/cms/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- STYLE CSS -->
     <link href="{{ secure_asset('assets/cms/assets/css/style.css') }}" rel="stylesheet">

	<!-- Plugins CSS -->
    <link href="{{ secure_asset('assets/cms/assets/css/plugins.css') }}" rel="stylesheet">

    <!--- FONT-ICONS CSS -->
    <link href="{{ secure_asset('assets/cms/assets/css/icons.css') }}" rel="stylesheet">

    <!-- INTERNAL Switcher css -->
    <link href="{{ secure_asset('assets/cms/assets/switcher/css/switcher.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('assets/cms/assets/switcher/demo.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body class="app sidebar-mini ltr light-mode">
    @include('cms.components.loader')
    <div class="page">
