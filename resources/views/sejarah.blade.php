@extends('components.empty')

@section('content')
    <div class="page__banner" data-background="{{ secure_asset('assets/img/bg/sejarah-banner.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__banner-title">
                        <h1 class="text-green">Sejarah Air Bambu</h1>
                        <div class="page__banner-title-menu">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product__page section-padding">
        <div class="container">
            <div class="row py-4">
                <div class="col">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/about/sejarah-1.png') }}" alt="logo-image">
                </div>
                <div class="col">
                    <div class="text-wrapper d-flex flex-column justify-content-center h-100">
                        <h2>Sekilas tentang Air Bambu Habiburrahman</h2>
                        <p>Berawal dari memperkenalkan air bambu dari masjid ke masjid, disini titik awal Pak Adang Meman
                            Sulaemanuddin S.Ag memulai bisnis Air Bambu Habiburrahman sejak tahun 2003. Brand Habiburrahman
                            diambil dari nama putra beliau yang memberikan pengalaman berharga tentang manfaat air bambu dan
                            memiliki arti "kekasih Allah".
                        </p>
                    </div>
                </div>
            </div>
            <div class="row py-4">
                <div class="col">
                    <div class="text-wrapper d-flex flex-column justify-content-center h-100">
                        <p>Air bambu sendiri sudah dipercaya dan menjadi obat alternatif alami dari tahun 2500 sebelum
                            masehi, hal ini tercantum dalam kitab Ming Yi Bie Lu yang merupakan kitab ribuan macam herbal
                            dan dari Kitab Herbal Mujarobat. Namun kini air bambu sudah dikemas dalam Air Bambu
                            Habiburrahman dengan kemasan yang higienis, tanpa campuran bahan apapun dan mudah dibawa kemana
                            pun. Air Bambu Habiburrahman siap mendampingi anda untuk tetap sehat alami
                        </p>
                    </div>
              </div>
                <div class="col">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/about/sejarah-2.jpg') }}" alt="logo-image">
                </div>
            </div>
            <div class="row py-4">
                <div class="col">
                    <img class="lazyload" data-src="{{ secure_asset('assets/img/about/sejarah-3.jpg') }}" alt="logo-image">
                </div>
                <div class="col">
                    <div class="text-wrapper d-flex flex-column justify-content-center h-100">
                        <p>Perjalanan Air Bambu Habiburrahman sejak tahun 2003 tidak pernah berhenti untuk memberikan bukti
                            bagaimana alam berkontribusi untuk menunjang hidup sehat dan alami. Air Bambu Habiburrahman akan
                            terus berdedikasi untuk menebarkan kebaikan dari kemurnian air bambu untuk anda dan hidup sehat
                            alami bersama Air Bambu Habiburrahman
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
