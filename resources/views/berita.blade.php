@extends('components.empty')

@php
use Carbon\Carbon;
@endphp

@section('content')
<div class="page__banner" data-background="assets/img/bg/page.jpg">
  <div class="container">
      <div class="row">
          <div class="col-xl-12">
              <div class="page__banner-title">
                  <h1>Blog Grid</h1>
                  <div class="page__banner-title-menu">
                      <ul>
                          <li><a href="#">Home</a></li>
                          <li><span>_</span>Blog Grid</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="blog__area section-padding">
  <div class="container">

    <div class="row">
      @foreach ($blog as $blg)
        <div class="col-xl-4 mb-30">
          <div class="blog__area-item">
            <div class="blog__area-item-image">
              <a href="blog-details.html"><img src="{{ Storage::url($blg->banner_image) }}" alt=""></a>
            </div>
            <div class="blog__area-item-content">
              <div class="blog__area-item-content-meta">
                <ul>
                  <li><a href="#"><i class="fal fa-calendar-alt"></i>{{ Carbon::parse($blg->created_at)->format('j F, Y'); }} </a></li>
                </ul>
              </div>
              <h4><a href="blog-details.html">{{ $blg->title }}</a></h4>
              <a href="{{ route('berita_detail', ['id' => $blg->slug]) }}" class="simple-btn text-success">Read More<i class="fa-solid fa-chevrons-right"></i></a>	
            </div>
          </div>
        </div>
      @endforeach
    </div>

    <div class="row">
      <div class="col-xl-12">
        <div class="theme__pagination t-center mt-50">
          <ul>
            <li><a class="active" href="#">01</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection