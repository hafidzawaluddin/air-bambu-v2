<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $blog = Blog::orderBy('created_at', 'DESC')->get();
        return view('berita', compact('blog'));
    }
    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $blog = Blog::where('slug', $id)->firstOrFail();
        return view('berita_detail', compact('blog'));
    }
}
