<?php

namespace App\Http\Controllers\Cms;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogCmsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $results = Blog::all();
        return view('cms.blog', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('cms.blog_create_edit');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $model = new Blog();
        $model->content = $request->content;
        $model->meta_description = $request->meta_description;
        $model->meta_keyword = $request->meta_keywords;
        $model->title = $request->title;
        $model->slug = str_replace(' ', '-', $request->title . '-' . uniqid());
        $model->banner_title = $request->banner_title;
        $model->writter = 'admin';
        $model->tags = json_encode($request->tags);

        if ($request->hasFile('header_image')) {
            $file = $request->file('header_image');

            $filename = uniqid() . '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/images', $filename);
            $model->header_image = $path;
        }

        if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');

            $filename = uniqid() . '.' . $file->getClientOriginalExtension();
            $pathbanner = $file->storeAs('public/images', $filename);
            $model->banner_image = $pathbanner;
        }

        $model->save();

        return redirect(route('cms.blog.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog)
    {
        return view('cms.blog_create_edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
