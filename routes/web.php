<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Cms\BlogCmsController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('/produk', function () {
    return view('produk');
})->name('produk');

Route::get('/berita', 'App\Http\Controllers\BlogController@index')->name('berita');
Route::get('/berita/{id}', 'App\Http\Controllers\BlogController@show')->name('berita_detail');

Route::get('/sejarah', function () {
    return view('sejarah');
})->name('sejarah');

Route::get('/sertifikasi', function () {
    return view('sertifikasi');
})->name('sertifikasi');

Route::prefix('cms')->name('cms.')->group(function () {
    Route::resource('blog', BlogCmsController::class); 
    
    Route::get('/dashboard', function () {
        // Uses first & second middleware...
    });
});

